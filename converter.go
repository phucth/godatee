package godatee

import (
        "time"
)

const (
        // format yyyy-mm-dd HH:ii:ss
        dateLayout = "2006-01-02 15:04:05"
)

// StringToUnixTime function to convert input string yyyy-mm-dd HH:ii:ss
// to Unix timestamp
func StringToUnixTime(tstr string) (t int64){
        tm, err := time.Parse(dateLayout, tstr)

        if err == nil {
                t = tm.Unix()
        }
        return
}


// TimeToString convert Unix time to string yyyy-mm-dd HH:ii:ss
func UnixTimeToString(t int64) string {
        if t == 0 {
                return time.Now().Format(dateLayout)
        }
        return time.Unix(t, 0).Format(dateLayout)
}
